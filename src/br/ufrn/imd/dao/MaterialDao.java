package br.ufrn.imd.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufrn.imd.dominio.Material;

@Stateless
public class MaterialDao  {

	@PersistenceContext
    private EntityManager em;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Material salvarOuAtualizar(Material material) {
		if(material.getId() == 0)
			em.persist(material);
		else
			em.merge(material);
		return material;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Material material) {
		material = em.find(Material.class, material.getId());
		em.remove(material);
	}
	
	@SuppressWarnings("unchecked")
	public List<Material> listar() {
		return (List<Material>) em.createQuery("select m from Material m").getResultList();
	}	
	
}
