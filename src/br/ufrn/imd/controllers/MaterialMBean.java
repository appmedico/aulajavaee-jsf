package br.ufrn.imd.controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;

import br.ufrn.imd.dao.MaterialDao;
import br.ufrn.imd.dominio.Material;

@ManagedBean
@SessionScoped
public class MaterialMBean {

	private Material material;
	
	private DataModel<Material> materiaisModel;
	
	@ManagedProperty(value="#{usuarioMBean}")
	private UsuarioMBean usuarioMBean;
	
	@Inject
	private MaterialDao materialDao;
	
	public MaterialMBean() {
		material = new Material();
	}
	
	public String novoMaterial() {
		material = new Material();
		return "/pages/material/form.jsf";
	}
	
	public String listarMateriais() {
		materiaisModel = new ListDataModel<Material>(materialDao.listar());
		return "/pages/material/list.jsf";
	}
	
	public String cadastrarMaterial() {
		material.setUsuarioCadastro(usuarioMBean.getUsuarioLogado());
		materialDao.salvarOuAtualizar(material);
		material = new Material();
		return "/pages/material/form.jsf";
	}
	
	public String removerMaterial() {
		Material materialRemovido = materiaisModel.getRowData();
		materialDao.remover(materialRemovido);
		materiaisModel = new ListDataModel<Material>(materialDao.listar());
		return "/pages/material/list.jsf";
	}
	
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public DataModel<Material> getMateriaisModel() {
		return materiaisModel;
	}

	public void setMateriaisModel(DataModel<Material> materiaisModel) {
		this.materiaisModel = materiaisModel;
	}

	public UsuarioMBean getUsuarioMBean() {
		return usuarioMBean;
	}

	public void setUsuarioMBean(UsuarioMBean usuarioMBean) {
		this.usuarioMBean = usuarioMBean;
	}
	
}
